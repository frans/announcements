<define-tag pagetitle>Neuer Archiv Snapshot Dienst verfügbar</define-tag>
<define-tag release_date>2010-04-12</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="1.1"

<p>Das Debian Projekt freut sich einen neuen Dienst &ndash; verfügbar über
<a href="http://snapshot.debian.org/">http://snapshot.debian.org/</a>
&ndash; ankündigen zu können. Es ist eine Zeitmaschine, die den Zugriff
auf alte Pakete basierend auf Datum oder Versionsnummer erlaubt.</p>

<p>Die Möglichkeit, Pakete und deren Quellen eines jeden beliebigem
Zeitpunktes zu installieren oder zu lesen, kann sowohl für Entwickler als
auch für Nutzer sehr hilfreich sein. Mit dieser Technik kann das Suchen
und Finden von Regressionen deutlich verbessert werden. Auch ist es
möglich, eine spezielle Paket-Umgebung bereit zu stellen, falls ein
Programm dies für den Betrieb benötigt. Das Snapshot-Archiv kann wie ein
normales Apt-Repository eingebunden und so einfach von Allen genutzt
werden.</p>

<p>Darüber hinaus ermöglicht das Snapshot-Repository die Installation
einer bekannten, getesteten Kombination von Paketen, in dem die
Versionen eines bestimmten Tages genutzt werden. Damit werden
Administratoren in die Lage versetzt, Upgrade-Pfade auf Staging-Systemen
zu testen und das Verfahren danach auf den Produktiv-Maschinen
umzusetzen &ndash; mit der Sicherheit, dass es sich um genau die gleichen
Schritte handelt. Da die Schnappschüsse das ganze Archiv umfassen, kann
dieses Verfahren nicht nur für Systeme genutzt werden, die Tests in einer
definierten Umgebung durchführen wollen, sondern auch Einsatzgebiete mit
strenger Versions-Kontrolle und -Freigabe, die anstehende Updates
umfangreich prüfen wollen, bevor sie diese freigeben.</p>

<p>Derzeit stellt das Archiv nahezu alle<a href="#1"><sup>[1]</sup></a>
Pakete des Debianarchivs und
und des Sicherheits-Archivs seit März 2005 bereit. Außerdem werden
ausgewählte zusätzliche Archive wie debian-volatile, debian-ports und
backports.org vorgehalten.  Dieser neue Dienst beherbergt bereits jetzt
6.5 TB Daten und wird kontinuierlich wachsen.</p>

<p>Das Hosting des Snapshot-Dienstes wird durch die <a
href="http://www.ece.ubc.ca/">Electrical and Computer Engineering Abteilung
der University of British Columbia</a> und dem <a
href="http://www.sanger.ac.uk/">Wellcome Trust Sanger Institute</a> bereit
gestellt. Zusätzliche Hardware wurde von <a
href="http://www.nordicgaming.com/">Nordic Gaming</a> gesponsert. Die
anfänglichen Daten kommen sowohl
vom Debian-ftp-master-Team als auch von Fumitoshi Ukai.</p>


<h2>Über Debian</h2>

<p>Debian GNU/Linux ist ein freies Betriebssystem, das von mehr als tausend
Freiwilligen aus der ganzen Welt entwickelt wird, die über das Internet
zusammenarbeiten. Debian widmet sich der Freien Software; seine
nicht-kommerzielle Natur und sein offenes Entwicklungsmodell machen es
einzigartig unter den GNU/Linux-Distributionen.</p>

<p>Die primären Stärken des Debian-Projekts liegen in der großen Basis
freiwilliger Mitarbeiter, seiner Hingabe zum Debian-Gesellschaftsvertrag
und seinem Engagement, das bestmögliche Betriebssystem
bereitzustellen.</p>

<p>Kontakt-Informationen: Für weitere Informationen besuchen Sie bitte
die Debian-Webseiten unter <a
href="http://www.debian.org/">http://www.debian.org/</a> oder schicken
Sie eine E-Mail an &lt;<a
href=mailto:press@debian.org">press@debian.org</a>&gt;.

<p><a name="1">[1]</a>
Einige Pakete wurden wegen Lizenzproblemen gelöscht, seit sie das erste
Mal freigegeben wurden.</p>
