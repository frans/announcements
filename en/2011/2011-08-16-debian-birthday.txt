Debian Community celebrates its 18th birthday

The Debian Project is pleased to mark the 18th anniversary of Ian Murdoch's
<a href="http://groups.google.com/group/comp.os.linux.development/msg/a32d4e2ef3bcdcc6">founding
announcement</a>.
Quoting from the <a href="http://www.debian.org/doc/manuals/project-history/ch-intro.en.html">official project history</a>: "The Debian Project was
officially founded by Ian Murdock on August 16th, 1993. At that time, 
the whole concept of a 'distribution' of Linux was new. Ian intended
Debian to be a distribution which would be made openly, in the spirit
of Linux and GNU."

A lot has happened to the project and its community in the past eighteen
years.  There have been eleven releases - most recently <a href="$(HOME)/News/2011/20110205a">
Debian 6.0 "Squeeze"</a> in February 2011 - and a huge amount of free software packaged.  The
current <q>unstable</q> branch consists of more than 35,000 binary packages
for the amd64 architecture alone - over 44GB of Free/Libre Software!
Throughout this history Debian has maintained its goals of technical excellence,
accountability, and above all freedom.

Of course that wouldn't be possible without the strong community which has
developed around Debian.  Besides more than 1,000 Debian Developers and
Maintainers from all over the globe, there are in excess of 11,000 registered accounts for
the <a href="http://alioth.debian.org/">Alioth collaboration platform</a>, and that
doesn't even include all those people contributing with translations or bug reports (and
sometimes patches for them) and all those users helping others via our
mailing lists, forums and IRC channels.

As a project, we would also like to take this opportunity to thank all our
users and contributors, and of course also our upstream developers. All of
them help to making Debian a great experience and a great project!

Across the world today, Debian Developers, Maintainers, Contributors and Users
<a href="http://wiki.debian.org/DebianDay2011">celebrate our birthday</a>
with small conferences, with virtual parties, and even
with parties in real life.  This celebration is organised by the
community, and it's not too late to start a party in your own city!

Users can also use the web platform at <a
href="http://thank-you.debian.net">http://thank-you.debian.net</a> to
express their thanks, and share pictures of their own Debian birthday
celebrations.  If you post on Twitter or Identi.ca please use the hashtag #thxdebian.

The Debian Project continues to welcome contributions in all forms,
from everyone, encouraging people to download, use, modify, 
and distribute its source code in the hopes that it is useful.
