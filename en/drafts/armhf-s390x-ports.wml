# Status: open-for-edit
# $Id$
# $Rev$

<define-tag pagetitle>Debian adds new armhf and s390x ports</define-tag>
<define-tag release_date>YYYY-MM-DD</define-tag>

#use wml::debian::news

<!--
Remaining issues:
Wait for ack from phil/#debian-release.
Friendly links for the s390 port? www.d.o/ports/?????
Credit the people responsible? credit debian-ports.org?? (Probably not, these are team efforts -- Sledge)
-->

<p>New armhf and s390x ports have been added to the Debian archive.
<insert timing-appropriate details here>.</p>

<p>The new <a href="http://wiki.debian.org/ArmHardFloatPort">armhf
port</a> targets a new ARM ABI aimed at newer ARM CPUs that support
hardware-accelerated floating-point. The port requires at least an
ARMv7 CPU using Thumb-2 and a VFP3-D16 FPU. The new ABI represents a
significant speed increase in floating-point intensive workloads for
users of devices containing ARM Cortex-series processors. The initial
set of armhf build machines use the Freescale i.MX53 CPU on Freescale
MX53 LOCO boards, donated by <a href="http://www.linaro.org/">Linaro</a>
and hosted by <a href="http://www.arm.com/">ARM</a> and <a
href="http://www.ynic.york.ac.uk/">York NeuroImaging Centre</a> at the
University of York. The older armel port is expected to continue into the
future to provide support for older ARM hardware which is still common.</p>

<p>The new <a href="???">s390x</a> port brings 64-bit addressing to our support for
IBM S/390 mainframes. 64-bit support was introduced to IBM mainframes in 2000 and all
IBM zSeries and System z mainframes support it. The port will be built by our
existing build machine for the 31-bit s390 port, which is hosted at 
<a href="http://www.zivit.de/">ZIVIT</a>. The s390x port is intended to supersede
the s390 port after the release of wheezy.</p>

<h2>About Debian</h2>

<p>
The Debian Project was founded in 1993 by Ian Murdock to be a truly
free community project. Since then the project has grown to be one of
the largest and most influential open source projects.  Thousands of
volunteers from all over the world work together to create and
maintain Debian software. Available in 70 languages, and
supporting a huge range of computer types, Debian calls itself the
<q>universal operating system</q>.
</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">http://www.debian.org/</a> or send mail to
&lt;press@debian.org&gt;.</p>
