# Status: [content-frozen]
# $Id: 2012-02-19-patent.wml 3162 2012-02-16 20:32:19Z madamezou $
# $Rev: 3162 $

<define-tag pagetitle>La posición de Debian ante las patentes de software</define-tag>

<define-tag release_date>2012-02-18</define-tag>
#use wml::debian::news


##
## Translators should uncomment the following line and add their name
## Leaving translation at 1.1 is okay; that's the first version which will
## be added to Debian's webwml repository
##
# #use wml::debian::translation-check translation="1.1" maintainer=""


<p>El proyecto Debian anuncia que ya está disponible su
<a href="$(HOME)/legal/patent">política de patentes/a> para el repositorio de Debian.</p>

<p>El proyecto Debian mantiene una posición crítica respecto a las patentes de software:
consideramos que las patentes de software son una amenaza para el software libre y un obstaculo
para la misión de Debian, que es proveer un sistema operativo completamente libre
para todos. Creemos que las patentes de software no permiten la innovación del software
y animamos a nuestros autores originales que rechazen las patentes de software.</p>

<p>A la vez, given the <em>de facto</em> possibility of patenting
software related ideas in several countries around the world, it is important
to neither underestimate nor overestimate software patent issues. We are
particularly concerned about patent <acronym title="fear, uncertainty, and
doubt">FUD</acronym> and we have worked to improve clarity on the subject.</p>

<p>After the publication of the <a href="$(HOME)/reports/patent-faq">Community
Distribution Patent Policy FAQ</a>, the availability of a patent policy for the
Debian archive is our next step in coping with the software patent system that
we hope to see abolished. We thank lawyers at
<a href="http://www.softwarefreedom.org">Software Freedom Law Center</a> for
working with us on this policy.</p>

<p><q>Patent Aggression is widespread throughout the information technology
industry at present</q>,﻿said Eben Moglen, founding director of the Software
Freedom Law Center. <q>Parties have spent billions of dollars trying to use
patent monopolies to halt innovation and threaten innovators. With the adoption
of this patent policy Debian prepares to defend its developers and users more
effectively, forcefully, and knowledgeably.</q></p>

<p>El lider del proyecto Debian, Stefano Zacchiroli dijo <q>El proyecto Debian tiene una
long tradition of standing up for users' rights to an entirely Free operating
system. Patent fears, uncertainties and doubts undermine this. A patent policy
and a contact point for related issues in the Debian archive will help reducing
patent FUD among our users.</q></p>


<h2>Acerca de Debian</h2>

<p>
El proyecto Debian fue fundado en 1993 por Ian Murdock para ser un proyecto
de la comunidad verdaderamente libre. Desde entonces el proyecto ha crecido
hasta ser uno de los proyectos más grandes e importantes de software libre.
Miles de voluntarios en todo el mundo trabajan juntos para crear y mantener
programas para Debian. Se encuentra traducido en 70 idiomas y soporta una gran
cantidad de arquitecturas de computadoras, por lo que el proyecto se refiere a
si mismo como <q>el sistema operativo universal</q>.
</p>


<h2>Información de contacto</h2>

<p>Para mayor información visite el sitio web de Debian en <a
href="http://www.debian.org/">http://www.debian.org/</a>, o envíe un correo a
&lt;press@debian.org&gt;.</p>
