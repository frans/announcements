<define-tag pagetitle>Primer versión de Debian Edu <q>Squeeze</q> publicada</define-tag>
<define-tag release_date>2012-03-11</define-tag>
#use wml::debian::news

##
## Translators should uncomment the following line and add their name
## Leaving translation at 1.1 is okay; that's the first version which will
## be added to Debian's webwml repository
##
# #use wml::debian::translation-check translation="1.1" maintainer=""

## first of all the news!
<p>
El equipo de Debian Edu se complace en anunciar el lanzamiento de 
Debian Edu <q>Squeeze</q> 6.0.4+r0!
## then a couple of info on what we are talking about
<br />
Debian Edu (<q>Skolelinux</q>) es una mezcla de Debian orientada
a escuelas e instituciones educativas, provee un entorno de red
completo pre-configurado para escuelas.
Contiene instalación y carga a través de PXE para maquinas sin disco,
y configuración para un servidor de escuela, estaciones de trabajo, y 
para estaciones de trabajo que pueden salir de la red de la escuela.
Muchas aplicaciones educativas como Celestia, Dr. Geo, GCompris,
GeoGebra, Kalzium, KGeography y Solfegeestán incluídas en la
configuración de escritorio por defecto.
</p>

## what is new in this release? just the most important things, for the
## rest let's link release notes!
<p>
Además de incluir las mejoras de la cuarta actualización
de Debian <q>Squeeze</q> (6.0.4), esta nueva publicación de Debian
Edu trae algunas mejoras interesantes, incluyendo:
reemplazo de LWAT por GOsa² como interfaz de administración LDAP;
arte gráfico actualizado y nuevo logo de Debian Edu / Skolelinux; nuevo entorno }
de escritorio LXDE, en adición a KDE (escritorio por defecto) y GNOME 
(LXDE y GNOME están disponibles en el disco de instalación); 
carga de cliente LTSP más rápida; mejora en el manejo de discos extraíbles 
en clientes ligeros; un nuevo perfil de estación de trabajo itinerante
para laptops; soporte completo para dominios Samba NT4 para Windows XP/Vista/7; etc.
<br />
El equipo de Debian Edu también ha trabajado intensamente en la documentación, 
mejorando y extendiendo el manual que ahora cuenta con traducciones completas a
Alemán, Francés e Italiano y traducciones parciales a Danés, Noruego y Español.
El proceso de instalación también ha sido mejorado, se ha integrado la nueva
versión de debian-installer, se ha permitido la copia de imagenes ISO
a memorias USB y se ha cambiado el particionamiento para instalaciones Standalone
y tener separado /home y no /usr.
</p>

# Add a nice quote
<p>
Cuando se preguntó sobre las 
<a href="http://people.skolelinux.org/pere/blog/Debian_Edu_interview__Nigel_Barker.html">ventajas
de Skolelinux/Debian Edu</a>, Nigel Barker respondió: 
<q>Para mi la configuración integrada.
No es solo el servidor o la estación de trabajo o LTSP.
Es todo junto y todo está configurado y listo para usar.
Leí en la documentacion que está diseñado para ser configuado
y manejado por profesores de ciencias y matemáticas, quienes no saben
mucho sobre computadoras, en una escuela pequeña de Noruega.
Eso me describe perfectamente si reemplazas Noruega con Japón</q>
</p>

## yeah, ok, this is all very interesting, but *how* can I get it? i.e.:
## instructions for downloading, installing, upgrading, ecc
<p>
Para quienes deseen probar Debian Edu <q>Squeeze</q>, 
<a href="http://maintainer.skolelinux.org/debian-edu-doc/en/debian-edu-squeeze-manual.html#Installation">instrucciones
completas de descarga e instalación</a> están disponibles con
instrucciones detalladas en el capitulo <a href="http://maintainer.skolelinux.org/debian-edu-doc/en/debian-edu-squeeze-manual.html#GettingStarted"><q>Iniciando
</q> del manual</a> donde se explica los primeros pasos sobre como configurar una red
o agregar usuarios.
<br />
Quienes todavía usen rc1-3 pueden actualizar a esta versión utilizando
<q>apt-get upgrade</q> - usuarios actulizando desde beta3 deben asegurarse 
de mantener el archivo gosa.conf actual cuando dpkg les pregunte durante
la actualización.
</p>

<p>
Las sumas sha1 de las imagenes ISO publicadas son:
</p> 
<pre>
  f4184237f0eb2a509c6729b3f8039b71f5f4394a  debian-edu-6.0.4+edu+r0-CD.iso
  64681588fffa7a20f5d9e67c726f010580e35b9f  debian-edu-6.0.4+edu+r0-DVD.iso
  087d0c69da17b4a98a2966ff752fcfea8e30ec23  debian-edu-6.0.4+edu+r0-source-DVD.iso
</pre>

<p>
¿Te gustaría alargar la vida de las computadoras de tu escuela?
¿Estás cansado de una administración a pie, de corer de computadora
a computadora re-instalando el sistema operativo? ¿Te gustaría
administrar todas las computadoras de tu escuela utilizando solo unas
horas cada semana? ¡Prueba Debian Edu Squeeze!
<br />
## then is time for some statistics/data 
Skolelinux es usado por al menos doscientas escuelas en todo el mundo,
la mayoría en Alemania (en el 2009 la región de Rhineland-Palatinate decidió usarlo
en todas sus escuelas) y Noruega. 
</p>

<h2>Acerca de Debian Edu</h2>

<p>
El proyecto <a href="http://www.skolelinux.org/">Skolelinux</a> fue
fundado en Noruega en el 2001 con el propósito de crear una distribución
GNU/Linux para escuelas y otras instituciones educativas.
Después de unirse al proyecto francés Debian Edu en 2003, Skolelinux
se convirtió en una <a href="http://wiki.debian.org/DebianPureBlends">Mezcla Pura de Debian</a>.
Hoy el sistema está en uso en muchos países en todo el mundo, con
más instalaciones en Noruega, España, Alemania y Francia. 
</p>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian fue fundado en 1993 por Ian Murdock para ser un proyecto
de la comunidad verdaderamente libre. Desde entonces el proyecto ha crecido
hasta ser uno de los proyectos más grandes e importantes de software libre.
Miles de voluntarios en todo el mundo trabajan juntos para crear y mantener
programas para Debian. Se encuentra traducido en 70 idiomas y soporta una gran
cantidad de arquitecturas de computadoras, por lo que el proyecto se refiere a
si mismo como «el sistema operativo universal».</p>


<h2>Información de contacto</h2>

<p>Para mayor información visite el sitio web de Debian en <a
href="http://www.debian.org/">http://www.debian.org/</a>, o envíe un correo a
&lt;press@debian.org&gt;.</p>


