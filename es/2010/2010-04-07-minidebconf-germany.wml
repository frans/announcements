<define-tag pagetitle>Primera Mini Conferencia de Debian en Alemania</define-tag>
<define-tag release_date>2010-04-08</define-tag>
#use wml::debian::news

<!-- This announcement has been released! http://www.debian.org/News/2010/20100408
  Please fix typoes / add translations only in Debians webwml cvs
  repository -->


<p>El proyecto Debian, el equipo encargado del sistema operativo libre llamado
Debian, se complace en anunciar que la primera Mini Conferencia de Debian en
Alemania tendrá lugar los días 10 y 11 de Junio en Berlín como parte del
evento LinuxTag de este año. LinuxTag es uno de los eventos de código abierto
más importantes en Europa, el cual se llevará a cabo este año del 9 al 12 de
Junio en el Centro de Convenciones de Berlín.</p>

<p>Como parte del programa para estos dos días se ofrecerán pláticas, talleres
y mesas de discusión para una audiencia que irá desde desarrolladores y
mantenedores, hasta contribuidores, usuarios y cualquier persona interesada.
Al mismo tiempo que la conferencia y junto a esta, tendrá lugar una «Bug
Squashing Party» en el <i>área de hacking</i>, destinada a corregir algunos
fallos críticos, realizar pruebas de actualización, así como también trabajar
en las notas de publicación de la próxima versión Debian 6.0 <q>Squeeze</q>.</p>

<p>No es necesario registrarse para asistir a la Mini Conferencia de Debian,
sin embargo, es necesario adquirir un boleto para el evento LinuxTag
(disponibles bajo solicitud). La información de este evento se encuentra en
<a href="http://wiki.debconf.org/wiki/Miniconf-LT-Berlin/2010">el Wiki de la
MiniDebConf</a>.</p>


<h2>Acerca de Debian</h2>

<p>El proyecto Debian fué creado en 1993 por Ian Murdock con la finalidad de
ser un proyecto libre y comunitario. Desde entonces el proyecto ha crecido
hasta convertirse en uno de los proyectos más grandes e influyentes entre los
proyectos de código abierto. Más de tres mil voluntarios de todo el mundo
colaboran para crear y mantener los programas en Debian. Traducido a más de 30
idiomas, y soportando una amplia gama de arquitecturas de computadoras, Debian
se llama a si mismo el <q>Sistema Operativo Universal</q>.</p>


<h2>Acerca de la MiniDebConf</h2>

<p>Una MiniDebConf es un evento en que se realizan conferencias por parte de
Desarrolladores del proyecto Debian, el cual es menor en número de asistentes
comparado con la DebConf, la cual se realiza cada año por el proyecto Debian.
Se planean pláticas de tipo técnico, social y político, por lo que una
MiniDebConf brinda la oportunidad a desarrolladores, contribuidores y cualquier
persona interesada, de conocerse y colaborar en persona. Al igual que la
DebConf, las MiniDebConfs se han realizado en múltiples ocasiones desde el año
2000 en distintos lugares del mundo como son: Europa, América, Asia y Oceanía.
</p>


<h2>Contacto</h2>

<p>Para mayor información, visite el sitio de Debian en
<a href="$(HOME)/">www.debian.org</a> o envíe un correo a
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt; ó &lt;<a
href="mailto:thommes@linuxtag.org">thommes@linuxtag.org</a>&gt;.</p>
